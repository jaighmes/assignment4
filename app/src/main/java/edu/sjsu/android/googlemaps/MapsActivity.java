package edu.sjsu.android.googlemaps;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import edu.sjsu.android.googlemaps.databinding.ActivityMapsBinding;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LoaderManager.LoaderCallbacks<Cursor> {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                Toast.makeText(getApplicationContext(),"Point added",Toast.LENGTH_SHORT).show();
                drawMarker(point);

                ContentValues contentValues = new ContentValues();
                contentValues.put(LocationsDB.latitude, point.latitude);
                contentValues.put(LocationsDB.longitude, point.longitude);
                contentValues.put(LocationsDB.zoomLevel, mMap.getCameraPosition().zoom);

                getContentResolver().insert(LocationsContentProvider.uri, contentValues);

                LocationInsertTask insertTask = new LocationInsertTask();
                insertTask.execute(contentValues);
            }
        });

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng point) {
                mMap.clear();
                LocationDeleteTask deleteTask = new LocationDeleteTask();
                deleteTask.execute();
                Toast.makeText(getApplicationContext(),"Deleted points",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private class LocationInsertTask extends AsyncTask<ContentValues, Void, Void> {
        @Override
        protected Void doInBackground(ContentValues... contentValues) {
            getContentResolver().insert(LocationsContentProvider.uri, contentValues[0]);
            return null;
        }
    }

    private class LocationDeleteTask extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            getContentResolver().delete(LocationsContentProvider.uri, null, null);
            return null;
        }
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = LocationsContentProvider.uri;
        return new CursorLoader(this, uri, null, null, null, null);
    }

    private void drawMarker(LatLng point){
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(point);
        mMap.addMarker(markerOptions);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int cnt = 0;
        double lat = 0;
        double lng = 0;
        float z = 0;

        cnt = data.getCount();

        if(data != null){
            data.moveToFirst();
        }
        else {
            cnt = 0;
        }

        for(int i = 0 ; i < cnt ; i++){
            // Get the latitude/long
            lat = data.getDouble(data.getColumnIndex(LocationsDB.latitude));
            lng = data.getDouble(data.getColumnIndex(LocationsDB.longitude));
            // Get zoom
            z = data.getFloat(data.getColumnIndex(LocationsDB.zoomLevel));

            LatLng location = new LatLng(lat, lng);
            drawMarker(location);
            data.moveToNext();
        }

        if ( cnt > 0 ) {
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), z);
            mMap.animateCamera(update);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        Cursor data = getContentResolver().query(LocationsContentProvider.uri, null, null, null,
                null);
        onLoadFinished(loader, data);
    }
}