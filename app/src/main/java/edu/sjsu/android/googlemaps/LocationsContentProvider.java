package edu.sjsu.android.googlemaps;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class LocationsContentProvider extends ContentProvider {

    public static String provider = "edu.sjsu.android.googlemaps";
    public static String url = "content://" + provider + "/locations";
    public static Uri uri = Uri.parse(url);
    LocationsDB db;
    static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(provider, "locations", 1);
    }

    @Override
    public boolean onCreate() {
        db = new LocationsDB(getContext());
        return true;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        db.insert(contentValues);
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        db.delete();
        return 0;
    }

    @Override
    public Cursor query(Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        return db.getLocations();
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }



    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

}
