package edu.sjsu.android.googlemaps;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import androidx.annotation.Nullable;

public class LocationsDB extends SQLiteOpenHelper {

    public static String databaseName = "LocationMapperDB";
    public static String tableName = "locations";
    public static String _id = "_id";
    public static String latitude = "latitude";
    public static String longitude = "longitude";
    public static String zoomLevel = "zoom";

    private SQLiteDatabase db;

    public LocationsDB(Context context) {
        super(context, databaseName, null, 1);
        this.db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql_table = "CREATE TABLE " + databaseName + " ( " +
                _id + " integer primary key autoincrement, " +
                longitude + " double not null, " +
                latitude + " double not null, " +
                zoomLevel + " integer not null" +
                " ) ";

        sqLiteDatabase.execSQL(sql_table);
    }

    // Insert location into database
    public long insert(ContentValues values) {
        long rowID = db.insert( tableName, null, values);
        return rowID;
    }

    // Delete location table data from database
    public int delete() {
        int res = db.delete(tableName, null, null);
        return res;
    }

    // Return all locations
    public Cursor getLocations() {
        return db.query(databaseName, new String[] { _id, latitude, longitude, zoomLevel },
                null, null, null, null, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}
